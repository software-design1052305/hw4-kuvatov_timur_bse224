package org.hse.software.authentication_service.model;

import org.springframework.security.core.GrantedAuthority;

public enum AuthorityType implements GrantedAuthority {
    ADMIN,
    DEFAULT;

    @Override
    public String getAuthority() {
        return this.name();
    }
}

