package org.hse.software.authentication_service.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.servlet.http.HttpServletRequest;
import org.hse.software.authentication_service.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


public interface TokenControllerInterface {

    @SecurityRequirement(name = "Bearer Authentication")
    @GetMapping("/validate-token")
    ResponseEntity<String> validateToken(HttpServletRequest request);

    @Operation
    @GetMapping("/get-user-info")
    ResponseEntity<User> getUserInfo(@RequestParam String token);
}
