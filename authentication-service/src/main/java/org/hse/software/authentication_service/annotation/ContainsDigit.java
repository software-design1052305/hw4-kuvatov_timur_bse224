package org.hse.software.authentication_service.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.hse.software.authentication_service.validator.ContainsDigitValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ContainsDigitValidator.class)
public @interface ContainsDigit {
    String message() default "Must contains a digit.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
