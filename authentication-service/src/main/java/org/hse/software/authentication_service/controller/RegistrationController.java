package org.hse.software.authentication_service.controller;

import jakarta.validation.Valid;
import org.hse.software.authentication_service.schema.RegistrationDetails;
import org.hse.software.authentication_service.service.UserService;
import org.hse.software.authentication_service.validator.UserRoleCodeValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController implements RegistrationControllerInterface {

    private final UserService userService;
    private final UserRoleCodeValidator userRoleCodeValidator;


    public RegistrationController(UserService userService, UserRoleCodeValidator userRoleCodeValidator) {
        this.userService = userService;
        this.userRoleCodeValidator = userRoleCodeValidator;
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@Valid @RequestBody RegistrationDetails registrationDetails) {
        registrationDetails.setRole(userRoleCodeValidator.validate(registrationDetails.getRoleCode()));
        userService.registerNewUser(registrationDetails);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}




