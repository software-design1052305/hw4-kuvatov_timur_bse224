package org.hse.software.authentication_service.converter;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.springframework.stereotype.Component;

import java.io.StringReader;
import java.security.PrivateKey;
import java.security.PublicKey;

@Component
public class RsaConverter {
    public PrivateKey convertRsaStringToPrivateKey(String stringPrivateKeyPEM) throws Exception {
        PEMParser pemParser = new PEMParser(new StringReader(stringPrivateKeyPEM));
        Object object = pemParser.readObject();
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider(new BouncyCastleProvider());
        return converter.getPrivateKey((PrivateKeyInfo) object);
    }

    public PublicKey convertRsaStringToPublicKey(String stringPublicKeyPEM) throws Exception {
        PEMParser pemParser = new PEMParser(new StringReader(stringPublicKeyPEM));
        Object object = pemParser.readObject();
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider(new BouncyCastleProvider());
        return converter.getPublicKey((SubjectPublicKeyInfo) object);
    }
}
