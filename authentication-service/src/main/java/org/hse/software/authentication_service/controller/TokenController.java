package org.hse.software.authentication_service.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.hse.software.authentication_service.model.User;
import org.hse.software.authentication_service.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController implements TokenControllerInterface {
    private final UserService userService;

    public TokenController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/validate-token")
    public ResponseEntity<String> validateToken(HttpServletRequest request) {
        String authorizationHeader = request.getHeader("Authorization");
        String jwtId = null;
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            jwtId = authorizationHeader.substring(7);
        }
        String jwt = userService.validateToken(jwtId);
        return ResponseEntity.ok(jwt);
    }


    @GetMapping("/get-user-info")
    public ResponseEntity<User> getUserInfo(@RequestParam String token) {
        User user;
        String jwtToken = userService.validateToken(token);
        user = userService.getUserInfo(jwtToken);
        return ResponseEntity.ok(user);
    }
}
