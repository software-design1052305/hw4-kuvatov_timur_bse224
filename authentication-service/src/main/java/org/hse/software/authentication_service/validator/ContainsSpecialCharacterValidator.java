package org.hse.software.authentication_service.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.hse.software.authentication_service.annotation.ContainsSpecialCharacter;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordData;
import org.passay.PasswordValidator;

import java.util.List;

public class ContainsSpecialCharacterValidator implements ConstraintValidator<ContainsSpecialCharacter, String> {

    @Override
    public boolean isValid(String text, ConstraintValidatorContext context) {
        PasswordValidator validator = new PasswordValidator(List.of(
                new CharacterRule(EnglishCharacterData.Special, 1)
        ));
        return validator.validate(new PasswordData(text)).isValid();
    }
}
