package org.hse.software.authentication_service.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.hse.software.authentication_service.annotation.NotEmailAlreadyExists;
import org.hse.software.authentication_service.repository.UserRepository;

public class NotEmailAlreadyExistsValidator implements ConstraintValidator<NotEmailAlreadyExists, String> {

    private final UserRepository userRepository;

    private NotEmailAlreadyExistsValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        return !userRepository.existsUserByEmail(email);
    }
}
