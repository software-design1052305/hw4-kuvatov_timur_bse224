package org.hse.software.authentication_service.validator;

import org.hse.software.authentication_service.model.AuthorityType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Objects;

@Component
public class UserRoleCodeValidator {

    private final HashMap<AuthorityType, String> codes = new HashMap<>();

    public UserRoleCodeValidator(@Value("${user.admin.secret.code}") String adminSecretCodes) {
        codes.put(AuthorityType.ADMIN, adminSecretCodes);
    }

    public AuthorityType validate(String code) {
        if (code.isEmpty()) {
            return AuthorityType.DEFAULT;
        }
        if (Objects.equals(codes.get(AuthorityType.ADMIN), code)) {
            return AuthorityType.ADMIN;
        } else {
            throw new IllegalArgumentException("The code is not valid.");
        }
    }
}
