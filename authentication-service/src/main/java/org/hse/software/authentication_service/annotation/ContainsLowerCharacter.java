package org.hse.software.authentication_service.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.hse.software.authentication_service.validator.ContainsLowerCharacterValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ContainsLowerCharacterValidator.class)
public @interface ContainsLowerCharacter {
    String message() default "Must contains lower character.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
