package org.hse.software.authentication_service.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Builder
@Entity
@Table(name = "service_user")
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    @JsonIgnore
    private Long userId;

    private String nickname;

    @JsonIgnore
    private String password;

    @Column(unique = true)
    private String email;

    @Column(name = "created_time")
    private LocalDateTime createdTime;

    @Enumerated(EnumType.STRING)
    private AuthorityType role;
}
