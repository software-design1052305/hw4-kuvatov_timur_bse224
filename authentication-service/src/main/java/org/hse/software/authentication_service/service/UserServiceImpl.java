package org.hse.software.authentication_service.service;

import org.hse.software.authentication_service.exception.ExpirationException;
import org.hse.software.authentication_service.model.Token;
import org.hse.software.authentication_service.model.User;
import org.hse.software.authentication_service.repository.TokenRepository;
import org.hse.software.authentication_service.repository.UserRepository;
import org.hse.software.authentication_service.schema.LoginDetails;
import org.hse.software.authentication_service.schema.RegistrationDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final TokenRepository tokenRepository;

    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    public UserServiceImpl(UserRepository userRepository, TokenRepository tokenRepository, PasswordEncoder passwordEncoder, JwtService jwtService) {
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
    }

    @Override
    public void registerNewUser(RegistrationDetails registrationDetails) {
        userRepository.save(User.builder().
                nickname(registrationDetails.getNickname()).email(registrationDetails.getEmail()).
                password(passwordEncoder.encode(registrationDetails.getPassword())).role(registrationDetails.getRole()).
                createdTime(LocalDateTime.now()).build());
    }

    @Override
    public String login(LoginDetails loginDetails) {
        Optional<User> optionalUser = userRepository.findUserByEmail(loginDetails.getEmail());
        if (optionalUser.isEmpty()) {
            throw new IllegalArgumentException("User with email " + loginDetails.getEmail() + " does not exist.");
        }
        User user = optionalUser.get();
        if (!passwordEncoder.matches(loginDetails.getPassword(), user.getPassword())) {
            throw new IllegalArgumentException("Incorrect password");
        }
        String jwt = jwtService.generateToken(user);
        Token token = Token.builder().jwtToken(jwt).build();
        tokenRepository.save(token);
        return token.getId();
    }

    @Override
    public User getUserInfo(String jwtToken) {
        return userRepository.findById(jwtService.extractUserId(jwtToken))
                .orElseThrow(() -> new IllegalArgumentException("Invalid token"));
    }

    @Override
    public String validateToken(String jwtId) {
        Optional<Token> optionalToken = tokenRepository.findById(jwtId);
        if (optionalToken.isEmpty()) {
            throw new IllegalArgumentException("Token " + jwtId + " doesn't exist");
        }
        String jwtToken = optionalToken.get().getJwtToken();
        if (!jwtService.validateToken(jwtToken)) {
            throw new ExpirationException("Token is expired");
        }
        return jwtToken;
    }
}
