package org.hse.software.authentication_service.controller;


import org.hse.software.authentication_service.schema.LoginDetails;
import org.hse.software.authentication_service.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController implements AuthenticationControllerInterface {

    private final UserService userService;

    public AuthenticationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginDetails loginDetails) {
        String jwtId;
        jwtId = userService.login(loginDetails);
        return new ResponseEntity<>(jwtId, HttpStatus.OK);
    }
}
