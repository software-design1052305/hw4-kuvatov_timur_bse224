package org.hse.software.authentication_service.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.hse.software.authentication_service.schema.LoginDetails;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface AuthenticationControllerInterface {

    @Operation
    @PostMapping("/login")
    ResponseEntity<String> login(@RequestBody LoginDetails loginDetails);
}
