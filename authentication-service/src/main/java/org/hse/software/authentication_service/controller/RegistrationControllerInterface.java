package org.hse.software.authentication_service.controller;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.hse.software.authentication_service.schema.RegistrationDetails;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface RegistrationControllerInterface {

    @Operation
    @PostMapping("/register")
    ResponseEntity<String> register(@Valid @RequestBody RegistrationDetails registrationDetails);
}
