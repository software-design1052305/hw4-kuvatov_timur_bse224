package org.hse.software.authentication_service.service;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.hse.software.authentication_service.converter.RsaConverter;
import org.hse.software.authentication_service.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtService {

    private final PrivateKey privateKey;
    private final PublicKey publicKey;

    private final Long expirationTime;

    public JwtService(RsaConverter rsaConverter, ResourceLoader resourceLoader, @Value("${jwt.expiration-time}") Long expirationTime) throws Exception {
        this.expirationTime = expirationTime;
        Resource privateKeyResource = resourceLoader.getResource("classpath:certificates/jwt-private.pem");
        privateKey = rsaConverter.convertRsaStringToPrivateKey(new String(privateKeyResource.getInputStream().readAllBytes(), StandardCharsets.UTF_8));
        Resource publicKeyResource = resourceLoader.getResource("classpath:certificates/jwt-public.pem");
        publicKey = rsaConverter.convertRsaStringToPublicKey(new String(publicKeyResource.getInputStream().readAllBytes(), StandardCharsets.UTF_8));
    }

    public Long extractUserId(String token) {
        return Long.parseLong(extractClaim(token, Claims::getSubject));
    }

    public String generateToken(User user) {
        HashMap<String, Object> claims = new HashMap<>();
        claims.put("username", user.getNickname());
        claims.put("role", user.getRole());
        return createToken(claims, user.getUserId());
    }

    private String createToken(Map<String, Object> claims, Long subject) {
        return Jwts.builder()
                .subject(subject.toString())
                .claims(claims)
                .issuedAt(new Date(System.currentTimeMillis()))
                .expiration(new Date(System.currentTimeMillis() + expirationTime))
                .issuer("spring-security")
                .signWith(privateKey)
                .compact();
    }

    public boolean validateToken(String token) {
        return !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }


    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser()
                .verifyWith(publicKey)
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }

}


