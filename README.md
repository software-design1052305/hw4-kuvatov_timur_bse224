# hw4-Kuvatov_Timur_BSE224

Система заказов по покупке билетов.

# Запуск

1. Создать образы микросервисов

```
cd ticket-service 
gradle clean build
docker build --no-cache -t ticket-service .
```

```
cd  authentication-service 
gradle clean build
docker build --no-cache -t  authentication-service .
```

```
cd  discovery-service 
gradle clean build
docker build --no-cache -t  discovery-service .
```

```
cd  gateway-service 
gradle clean build
docker build --no-cache -t  gateway-service .
```

2. Запустить контейнеры

```
cd  dev-env 
docker compose up
```

3. Открыть документацию

После запуска последнего микросервиса подождать примерно 40с. до момента, когда discovery сервис обнаружит все сервисы.

Скопировать содержимое файла по пути: \
 /gateway-service/src/main/resources/static/open-api.json

Вставить содержимое файла в https://editor.swagger.io/

При возникновении ошибки "Semantic error at components.securitySchemes.Bearer Authentication
Component names can only contain the characters A-Z a-z 0-9 - . _
Jump to line 302" перезапустить страницу.


## Описание

Пользователь отправляет запрос на регистрацию, затем на авторизацию, получая id токена, который соответвует jwt токену в Redis микросервиса авторизации, который был зашифрован private key.

При отправке запросов на микросервис по покупке билетов, происходит предварительная фильтрация запроса: отправляется запрос на валидацию токена по его id в  микросервис авторизации. В случае успешной валидации в gateway-микросервис возвращается jwt токен, который добавляется в заголовок запроса к микросервису по покупке билетов, где токен с помощью public key засшифровывается.

Обнаружение микросервисов происходит за счёт dicovery-service.

## Стек

Spring Boot, Hibernate, Spring Cloud Gateway, Redis, PostgreSQL, Liquibase, Eureka, OpenApi.