package org.hse.software.gateway_service.config;

import org.hse.software.gateway_service.filter.AuthenticationPrefilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProxyConfig {

    @Bean
    public RouteLocator routes(
            RouteLocatorBuilder builder, AuthenticationPrefilter authenticationPrefilter) {
        return builder.routes()
                .route("authentication-service-route", r -> r.path("/authentication-service/**")
                        .filters(f ->
                                f.rewritePath("/authentication-service(?<segment>/?.*)", "$\\{segment}"))

                        .uri("lb://authentication-service"))
                .route("ticket-service-route", r -> r.path("/ticket-service/**")
                        .filters(f ->
                                f.rewritePath("/ticket-service(?<segment>/?.*)", "$\\{segment}")
                                        .filter(authenticationPrefilter.apply(
                                                new AuthenticationPrefilter.Config())))
                        .uri("lb://ticket-service"))
                .build();
    }

   
}
