\c

create user "postgres-user" with password 'postgres-password';

create database ticket_service_db with owner = 'postgres';

\c ticket_service_db

create schema ext;

grant usage on schema ext to "postgres-user";
alter role "postgres-user" set search_path = template, ext, public;

create extension "uuid-ossp" schema ext;
create schema template authorization "postgres-user";

