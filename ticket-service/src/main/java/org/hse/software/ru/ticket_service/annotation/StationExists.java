package org.hse.software.ru.ticket_service.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.hse.software.ru.ticket_service.validator.StationExistsValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StationExistsValidator.class)
public @interface StationExists {
    String message() default "Station does not exist";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
