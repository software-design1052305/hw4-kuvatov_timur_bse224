package org.hse.software.ru.ticket_service.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Builder
@Table(name = "ticket_order")
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;
    @Column(name = "user_id")
    private Long userId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "from_station_id")
    @JsonManagedReference
    private Station fromStation;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "to_station_id")
    @JsonManagedReference
    private Station toStation;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @Column(name = "created_time")
    private LocalDateTime createdTime;

}

