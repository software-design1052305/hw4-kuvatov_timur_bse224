package org.hse.software.ru.ticket_service.schema;

import lombok.Data;

@Data
public class StationIn {
    private String stationName;
}
