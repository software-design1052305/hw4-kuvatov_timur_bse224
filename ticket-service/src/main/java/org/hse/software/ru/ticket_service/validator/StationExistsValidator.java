package org.hse.software.ru.ticket_service.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.hse.software.ru.ticket_service.annotation.StationExists;
import org.hse.software.ru.ticket_service.repository.StationRepository;

public class StationExistsValidator implements ConstraintValidator<StationExists, String> {

    private final StationRepository stationRepository;

    private StationExistsValidator(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    @Override
    public boolean isValid(String stationName, ConstraintValidatorContext context) {
        return stationRepository.existsStationByName(stationName);
    }
}
