package org.hse.software.ru.ticket_service.converter;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.springframework.stereotype.Component;

import java.io.StringReader;
import java.security.PublicKey;

@Component
public class RsaConverter {
    public PublicKey convertRsaStringToPublicKey(String stringPublicKeyPEM) throws Exception {
        PEMParser pemParser = new PEMParser(new StringReader(stringPublicKeyPEM));
        Object object = pemParser.readObject();
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider(new BouncyCastleProvider());
        return converter.getPublicKey((SubjectPublicKeyInfo) object);
    }
}
