package org.hse.software.ru.ticket_service.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Builder
@Table(name = "station")
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
@Setter
@Getter
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(unique = true)
    private String name;
}
