package org.hse.software.ru.ticket_service.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hse.software.ru.ticket_service.annotation.StationExists;

@Data
public class OrderIn {
    @JsonIgnore
    private Long userId;
    @NotBlank
    @StationExists(message = "From station name does not exists")
    private String fromStationName;
    @NotBlank
    @StationExists(message = "To station name does not exists")
    private String toStationName;
}
