package org.hse.software.ru.ticket_service.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.hse.software.ru.ticket_service.model.Order;
import org.hse.software.ru.ticket_service.schema.OrderIn;
import org.hse.software.ru.ticket_service.schema.StationIn;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface OrderControllerInterface {

    @Operation(description = "Create order")
    @SecurityRequirement(name = "Bearer Authentication")
    @PostMapping("order/create")
    ResponseEntity<String> createOrder(@Valid @RequestBody OrderIn orderIn);

    @Operation(description = "Add station")
    @SecurityRequirement(name = "Bearer Authentication")
    @PostMapping("add/station")
    ResponseEntity<String> addStation(@RequestBody StationIn stationIn);


    @Operation(description = "Add station")
    @SecurityRequirement(name = "Bearer Authentication")
    @GetMapping("order/get/{id}")
    ResponseEntity<Order> getOrder(@PathVariable(name = "id") Long orderId);
}
