package org.hse.software.ru.ticket_service.controller;

import jakarta.validation.Valid;
import org.hse.software.ru.ticket_service.model.Order;
import org.hse.software.ru.ticket_service.model.TicketServiceUserDetails;
import org.hse.software.ru.ticket_service.schema.OrderIn;
import org.hse.software.ru.ticket_service.schema.StationIn;
import org.hse.software.ru.ticket_service.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController implements OrderControllerInterface {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("order/create")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'DEFAULT')")
    public ResponseEntity<String> createOrder(@Valid @RequestBody OrderIn orderIn) {
        TicketServiceUserDetails userDetails = (TicketServiceUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        orderIn.setUserId(userDetails.getUserId());
        Order order = orderService.createOrder(orderIn);
        return new ResponseEntity<>("{\"id\":" + order.getId() + "}", HttpStatus.OK);
    }

    @PostMapping("add/station")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<String> addStation(@RequestBody StationIn stationIn) {
        orderService.addStation(stationIn);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @GetMapping("order/get/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'DEFAULT')")
    public ResponseEntity<Order> getOrder(@PathVariable(name = "id") Long orderId) {
        Order order = orderService.getOrder(orderId);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}
