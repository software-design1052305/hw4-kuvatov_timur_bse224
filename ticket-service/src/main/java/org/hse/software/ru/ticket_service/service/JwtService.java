package org.hse.software.ru.ticket_service.service;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.hse.software.ru.ticket_service.converter.RsaConverter;
import org.hse.software.ru.ticket_service.model.AuthorityType;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.function.Function;

@Service
public class JwtService {

    private final PublicKey publicKey;

    public JwtService(RsaConverter rsaConverter, ResourceLoader resourceLoader) throws Exception {
        Resource publicKeyResource = resourceLoader.getResource("classpath:certificates/jwt-public.pem");
        publicKey = rsaConverter.convertRsaStringToPublicKey(new String(publicKeyResource.getInputStream().readAllBytes(), StandardCharsets.UTF_8));
    }

    public Long extractUserId(String token) {
        return Long.parseLong(extractClaim(token, Claims::getSubject));
    }

    public String extractUsername(String token) {
        return extractClaim(token, claims -> claims.get("username", String.class));
    }

    public AuthorityType extractRole(String token) {
        return AuthorityType.valueOf(extractClaim(token, claims -> claims.get("role", String.class)));
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser()
                .verifyWith(publicKey)
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }
}


