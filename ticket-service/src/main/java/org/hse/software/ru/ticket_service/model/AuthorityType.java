package org.hse.software.ru.ticket_service.model;

import org.springframework.security.core.GrantedAuthority;

public enum AuthorityType implements GrantedAuthority {
    ADMIN,
    DEFAULT;

    @Override
    public String getAuthority() {
        return this.name();
    }
}

