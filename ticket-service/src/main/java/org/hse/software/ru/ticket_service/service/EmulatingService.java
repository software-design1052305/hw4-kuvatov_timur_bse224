package org.hse.software.ru.ticket_service.service;

import jakarta.transaction.Transactional;
import org.hse.software.ru.ticket_service.model.Order;
import org.hse.software.ru.ticket_service.model.OrderStatus;
import org.hse.software.ru.ticket_service.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component
public class EmulatingService {

    private final OrderRepository orderRepository;

    @Value("${scheduling.rejection-probability}")
    private double rejectionProbability;

    public EmulatingService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Scheduled(fixedRateString = "${scheduling.delta-time}")
    @Transactional
    public void processTickets() {
        List<Order> orders = orderRepository.findOrdersByStatus(OrderStatus.CHECK);
        if (orders.isEmpty()) {
            return;
        }
        Random random = new Random();
        Order randomOrder = orders.get(random.nextInt(orders.size()));
        randomOrder.setStatus((random.nextDouble() <= rejectionProbability ? OrderStatus.REJECTION : OrderStatus.SUCCESS));
    }
}
