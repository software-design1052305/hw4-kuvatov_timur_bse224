package org.hse.software.ru.ticket_service.service;

import org.hse.software.ru.ticket_service.model.Order;
import org.hse.software.ru.ticket_service.model.OrderStatus;
import org.hse.software.ru.ticket_service.model.Station;
import org.hse.software.ru.ticket_service.repository.OrderRepository;
import org.hse.software.ru.ticket_service.repository.StationRepository;
import org.hse.software.ru.ticket_service.schema.OrderIn;
import org.hse.software.ru.ticket_service.schema.StationIn;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final StationRepository stationRepository;

    private OrderServiceImpl(OrderRepository orderRepository, StationRepository stationRepository) {
        this.orderRepository = orderRepository;
        this.stationRepository = stationRepository;
    }

    @Override
    public Order createOrder(OrderIn orderIn) {
        Station fromStation = stationRepository.findStationByName(orderIn.getFromStationName());
        Station toStation = stationRepository.findStationByName(orderIn.getToStationName());
        Order order = Order.builder().fromStation(fromStation).toStation(toStation).
                userId(orderIn.getUserId()).createdTime(LocalDateTime.now()).
                status(OrderStatus.CHECK).build();
        return orderRepository.save(order);
    }

    @Override
    public void addStation(StationIn stationIn) {
        if (stationRepository.existsStationByName(stationIn.getStationName())) {
            throw new IllegalArgumentException("Station with the same name already exists.");
        }
        stationRepository.save(Station.builder().name(stationIn.getStationName()).build());
    }

    @Override
    public Order getOrder(Long orderId) {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isEmpty()) {
            throw new IllegalArgumentException("Incorrect order id.");
        }
        return optionalOrder.get();
    }
}

