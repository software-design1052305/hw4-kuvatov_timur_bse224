package org.hse.software.ru.ticket_service.service;

import org.hse.software.ru.ticket_service.model.Order;
import org.hse.software.ru.ticket_service.schema.OrderIn;
import org.hse.software.ru.ticket_service.schema.StationIn;
import org.springframework.stereotype.Service;

@Service
public interface OrderService {
    Order createOrder(OrderIn orderIn);

    void addStation(StationIn stationIn);

    Order getOrder(Long orderId);
}
